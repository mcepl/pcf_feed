#!/usr/bin/python3
import argparse
import datetime
import locale
import logging
import re

from pprint import pformat, pprint
import lxml.etree as ET  # noqa
# for pretty printing XML see https://bugs.python.org/issue14465
# https://bugs.python.org/file31168/issue14465.patch
# import xml.etree.ElementTree as ET

from html.parser import HTMLParser
from urllib.request import urlopen
from urllib.parse import quote as urlquote
OUT_ENC = "unicode"


# logging.basicConfig(level=logging.DEBUG)
PCF_URL = "http://www.praguefellowship.cz/pages/Default.aspx?tabid=217"
DC_NS = "http://purl.org/dc/elements/1.1/"
ATOM_NS = "http://www.w3.org/2005/Atom"
ITUNES_NS = "http://www.itunes.com/dtds/podcast-1.0.dtd"
ET.register_namespace('atom', '{}'.format(ATOM_NS))
ET.register_namespace('dc', '{}'.format(DC_NS))
ET.register_namespace('itunes', '{}'.format(ITUNES_NS))

class PCFMP3SermonsParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__in_item = False
        self.result_list = []
        self.__collected_data = None
        self.__item = {
            'links': [],
            'data': []
        }

    @staticmethod
    def __get_encoding(page_info):
        """
        Determine encoding of the page from its headers.

        Should be both 2and3 compliant.

        In case of doubts, defaults to 'utf-8'.
        """
        if hasattr(page_info, 'get_content_charset'):
            return page_info.get_content_charset()

        header = page_info.getheader('content-type')
        if header is not None:
            hdr_split = header.split('charset=')
            if len(hdr_split) > 1:
                return hdr_split[1]

        return 'utf-8'

    def feed(self, instr):
        enc = self.__get_encoding(instr.info())
        HTMLParser.feed(self, instr.read().decode(enc))

    def handle_starttag(self, tag, attrs):
        logging.debug('tag = %s, attrs = %s', tag, attrs)
        logging.debug('self.__in_item = %s, self.__collected_data = %s',
                      self.__in_item, self.__collected_data)
        attrs = dict(attrs)
        if tag == 'tr' and 'class' in attrs and attrs['class'] == 'Normal':
            logging.debug('setting self.__in_item to True')
            self.__in_item = True
        elif tag in ['td', 'th'] and self.__in_item:
            self.__collected_data = ''
        else:
            if self.__in_item:
                if tag == 'a':
                    self.__item['links'].append(attrs['href'])
        logging.debug('self.__item = %s', self.__item)

    def handle_endtag(self, tag):
        logging.debug('tag = %s', tag)
        if tag == 'tr':
            logging.debug('setting self.__in_item to False')
            self.__in_item = False
            if (len(self.__item['links']) > 0) or \
                    (len(self.__item['data']) > 0):
                logging.debug('self.__item = %s', self.__item)
                self.result_list.append(self.__item)
                self.__collected_data = None
                logging.debug('self.result_list = %s', self.result_list)
            self.__item = {
                'links': [],
                'data': []
            }
        elif self.__collected_data is not None:
            self.__item['data'].append(self.__collected_data.strip())
            self.__collected_data = None

    def handle_data(self, data):
        # FIXME collect data only inside of <td> elements and
        # add all of them in one element together
        data = data.strip()
        logging.debug('data = %s', data)
        if self.__in_item and self.__collected_data is not None:
            logging.debug('data = %s', data)
            self.__collected_data += data + " "
        logging.debug('complete self.__collected_data = %s',
                      self.__collected_data)


def generate_header():
    """
    Generate some kind of lxml object with the generally applicable
    mandatory elements included.
    """
    root = ET.Element('rss', attrib={'version': '2.0'})
    chan = ET.SubElement(root, 'channel')
    ET.SubElement(chan, 'title').text = 'PCF Sermons Feed'
    ET.SubElement(chan, 'link').text = PCF_URL
    ET.SubElement(chan, 'description').text = \
        'Podcast of all sermons from PCF as MP3 files'
    ET.SubElement(chan, "{%s}link" % ATOM_NS, attrib={
        'rel': 'self',
        'type': "application/rss+xml",
        'href': 'http://matej.ceplovi.cz/progs/data/PCF_feed.xml'})
    return root, chan


def extract_enclosure(ini, extension, label, media_type):
    encl_url = None

    if len(ini['links']) > 0:
        mp3_urls = [link for link in ini['links'] if link.endswith(extension)]

        if len(mp3_urls) > 0:
            encl_url = mp3_urls[0]
            mp3_idx = ini['links'].index(encl_url)
            del ini['links'][mp3_idx]
            # Enclosure found! Filter out the label
            ini['data'] = [d for d in ini['data'] if label not in d.lower()]

    if encl_url is None:
        return None

    return {'url': urlquote(encl_url, ':/'),
            'mime': media_type}


def generate_item(ini):
    logging.debug("%s\nbefore ini = %s\n", '=' * 30, pformat(ini))

    if ini['data'][0].strip() == '':
        del ini['data'][0]

    logging.debug("ini['data'] = %s", ini['data'])
    logging.debug("ini['data'][0] = %s", len(ini['data'][0]))

    # Feb 02 2014
    if len(ini['data'][0]) == 0:
        return None

    date = datetime.datetime.strptime(ini['data'][0], '%b %d %Y')
    del ini['data'][0]
    logging.debug('date = %s', date)

    # find enclosure
    enclosure = extract_enclosure(ini, '.mp3', 'stream', 'audio/mpeg')
    # throw away PDF and M3U playlist
    extract_enclosure(ini, '.pdf', 'download pdf', 'application/pdf')
    extract_enclosure(ini, '.m3u', 'download', 'audio/x-mpegurl')
    # Just eliminate 'Video Clip' labels
    ini['data'] = [
        d for d in ini['data']
        if not re.match(r'((communion|funny) )?video( (clip|testimony))?',
                        d, flags=re.IGNORECASE)]

    logging.debug('enclosure = %s', enclosure)
    if enclosure is None:
        return None

    logging.debug("after de-enclosure ini = %s\n", pformat(ini['data']))

    author = ini['data'][0]
    del ini['data'][0]

    # this is kind of desperate, but I don't know much better
    title = " ".join(ini['data']).strip()

    out = ET.Element('item')
    ET.SubElement(out, 'title').text = title
    ET.SubElement(out, '{%s}creator' % DC_NS).text = author
    ET.SubElement(out, '{%s}author' % ITUNES_NS).text = author
    ET.SubElement(out, '{%s}explicit' % ITUNES_NS).text = 'clean'
    ET.SubElement(out, 'pubDate').text = \
        date.strftime("%a, %d %b %Y %H:%M:%S +0100")
    ET.SubElement(out, 'enclosure', attrib={
        'url': enclosure['url'],
        'length': "0",
        'type': enclosure['mime']})
    logging.debug('out:\n%s', ET.tostring(out))
    return out


def generate_feed(inlist):
    root, ch = generate_header()
    logging.debug('root = %s', ET.tostring(root))

    for in_item in inlist:
        item = generate_item(in_item)
        if item is not None:
            logging.debug('item:\n%s', ET.tostring(item))
            ch.append(item)
            logging.debug('ch:\n%s', ET.tostring(ch))

    logging.debug('root = %s', ET.tostring(root))
    return root


if __name__ == '__main__':
    locale.setlocale(locale.LC_ALL, ('en_US', 'utf-8'))
    encoding = None

    arg_parser = argparse.ArgumentParser(
        description='Generate RSS feed from the PCF MP3 sermon page.')
    arg_parser.add_argument('-v', '--verbose', action='store_true')
    arg_parser.add_argument('filename')
    args = arg_parser.parse_args()

    if args.verbose:
        logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                            level=logging.DEBUG)

    logging.debug('args = {0}'.format(args))

    feed_parser = PCFMP3SermonsParser()
    instream = urlopen(PCF_URL)

    feed_parser.feed(instream)
    instream.close()

    logging.debug('feed_parser.result_list:\n%s',
                  pformat(feed_parser.result_list))

    feed = generate_feed(feed_parser.result_list)
    with open(args.filename, 'w', encoding='utf-8') as feed_file:
        feed_file.write(ET.tostring(feed, encoding=OUT_ENC,
                                    pretty_print=True))
